package com.certisign.revocation.adapter.dto

import com.certisign.revocation.adapter.repository.model.RevokeRecord
import com.certisign.revocation.domain.model.Revoke
import io.swagger.annotations.ApiModelProperty
import io.swagger.annotations.ApiParam
import org.springframework.format.annotation.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

data class RevokeDTO (
    var connectionId: String,
    var credExId: String,
    var notify: Boolean,
    var publish: Boolean,
    var threadId: String,
    var expiration: String,
    var revoked: Boolean
    ) {

    companion object {
        @JvmStatic
        fun from(revokeRecord: RevokeRecord): RevokeDTO {

            requireNotNull(revokeRecord.credExId) {"Parameter 'revokeDto' has invalid id"}
            requireNotNull(revokeRecord.expiration) {"Parameter 'credExpiration' is required"}

            val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            var stringDate = fmt.format(revokeRecord.expiration);

            return RevokeDTO(
                connectionId = revokeRecord.connectionId,
                credExId = revokeRecord.credExId,
                notify = revokeRecord.notify,
                publish = revokeRecord.publish,
                threadId = revokeRecord.threadId,
                expiration = stringDate,
                revoked = revokeRecord.revoked
            )
        }
    }
}