package com.certisign.revocation.adapter.repository

import com.certisign.revocation.adapter.repository.model.RevokeRecord
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RevokeRepositoryMongo : MongoRepository<RevokeRecord, String> {

    fun findByCredExId(credExId: String) : RevokeRecord?
    fun findByExpiration(date: Date): List<RevokeRecord>

}