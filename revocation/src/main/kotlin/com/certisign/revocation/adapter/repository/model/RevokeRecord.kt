package com.certisign.revocation.adapter.repository.model

import com.certisign.revocation.domain.model.Revoke
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.format.annotation.DateTimeFormat
import java.util.*

@Document("revocations")
class RevokeRecord (

    @Id
    var id: String,
    var connectionId: String,
    var credExId: String,
    var notify: Boolean,
    var publish: Boolean,
    var revoked: Boolean = false,
    var threadId: String,

    @DateTimeFormat(iso = DateTimeFormat.ISO.NONE, pattern = "yyyy-MM-dd HH:mm:ss")
    var expiration: Date

){
    companion object {
        @JvmStatic
        fun from(revoke: Revoke): RevokeRecord {
            requireNotNull(revoke.credExId()) {"Parameter 'revoke' has invalid id"}
            return RevokeRecord(
                id = UUID.nameUUIDFromBytes(revoke.credExId().toByteArray()).toString(),
                connectionId = revoke.connectionId(),
                credExId = revoke.credExId(),
                notify = revoke.notify(),
                publish = revoke.publish(),
                threadId = revoke.threadId(),
                expiration = revoke.expiration(),
                revoked = revoke.revoked()
            )
        }
    }
}