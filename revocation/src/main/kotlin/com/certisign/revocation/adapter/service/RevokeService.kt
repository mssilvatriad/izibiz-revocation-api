package com.certisign.revocation.adapter.service

import com.certisign.revocation.adapter.dto.RevokeDTO
import com.certisign.revocation.domain.exception.AlreadyRegisteredException
import com.certisign.revocation.domain.exception.NewRevocationRegistryException
import com.certisign.revocation.domain.exception.RetrieveRevokingException
import com.certisign.revocation.domain.exception.RevokeCredentialAriesException
import com.certisign.revocation.domain.model.Revoke
import com.certisign.revocation.domain.model.RevokeRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@Service
class RevokeService (
    private val revokeRepository: RevokeRepository,
    private val ariesService: AriesService
){

    fun registerRevoke(revoke: Revoke): String {

        if (revoke.expiration() != null)
            revoke.setExpiration(parseDateFormat(revoke.expiration()));

       //Check if credential already exists on database
        if (this.revokeRepository.findByCredExId(revoke.credExId()).isPresent)
            throw AlreadyRegisteredException("This credential has been registered already");

        return revokeRepository.save(revoke)
    }

    fun findAll(): ArrayList<RevokeDTO> {
        try {
            var revokes = arrayListOf<RevokeDTO>();
            revokeRepository.findAll().stream().forEach { r ->
                run {
                    var dto = RevokeDTO.from(r)
                    if (dto.credExId != null) {
                        revokes.add(dto)
                    }
                }
            }
            return revokes;
        } catch (e: RetrieveRevokingException) {
            throw RetrieveRevokingException("Error when retrieve all revokes")
        }
    }

    fun findAllExpiration(date: String): ArrayList<RevokeDTO> {
        try {
            var parsedDate = parseData(date);
            var revokes = arrayListOf<RevokeDTO>();

            if (parsedDate != null) {
                var revocations = revokeRepository.findByExpiration(parsedDate);
                revocations.stream().forEach { r ->
                    run {
                        var dto = RevokeDTO.from(r)
                        if (dto.credExId != null) {
                            revokes.add(dto)
                        }
                    }
                }
            }
            return revokes;
        } catch (e: RetrieveRevokingException) {
            throw RetrieveRevokingException("Error when retrieve revokes with this data parameter!")
        }
    }

    fun revokeCredentialAries(date: String): ArrayList<RevokeDTO> {
        var revocations = arrayListOf<RevokeDTO>();
        try {
            findAllExpiration(date).stream().forEach { r ->
                run {
                    if (!r.revoked) {
                        val updatedRevoke = ariesService.revokeCredential(r);
                        if (updatedRevoke.isPresent) {
                            revocations.add(RevokeDTO.from(updatedRevoke.get()))
                        }
                    }
                }
            }
            return revocations
        } catch (e: RevokeCredentialAriesException) {
            throw RevokeCredentialAriesException("Error when revoke the credential on Aries!")
        }

    }

    fun parseData(data: String): Date? {
        val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        var newDate = fmt.parse(data);
        return newDate;
    }

    fun parseDateFormat(date: Date): Date {
        val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        var stringDate = fmt.format(date);
        var newDate = fmt.parse(stringDate);
        return newDate;
    }
}