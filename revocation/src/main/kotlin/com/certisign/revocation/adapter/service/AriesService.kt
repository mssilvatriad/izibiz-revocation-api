package com.certisign.revocation.adapter.service

import com.certisign.revocation.adapter.dto.RevokeDTO
import com.certisign.revocation.adapter.repository.model.RevokeRecord
import com.certisign.revocation.domain.exception.RevokingException
import com.certisign.revocation.domain.model.Revoke
import com.certisign.revocation.domain.model.RevokeRepository
import io.swagger.client.ApiClient
import io.swagger.client.api.RevocationApi
import io.swagger.client.model.RevRegsCreated
import io.swagger.client.model.RevocationModuleResponse
import io.swagger.client.model.RevokeRequest
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class AriesService(private val revokeRepository: RevokeRepository) {

    @Value("\${aries.cloud.url}")
    private val ARIES_BASEPATH: String = ""

    fun searchAriesRevocations(): RevRegsCreated? {
        val revocation = RevocationApi(getClient())
        val revs = revocation.revocationRegistriesCreatedGet(null, null);
        return revs
    }

    fun revokeCredential(revokeDto : RevokeDTO): Optional<RevokeRecord> {
        try {
            var revocation = RevocationApi(getClient())
            var body = toRevokeRequest(revokeDto);
            var revoke = revocation.revocationRevokePostWithHttpInfo(body);

            return if (revoke.statusCode == 200) {
                changeRevokedStatus(revokeDto);
            } else {
                Optional.empty();
            }
        } catch (e: RevokingException) {
            throw RevokingException("Error when revoking the credential!")
        }
    }

    fun changeRevokedStatus(revoked: RevokeDTO): Optional<RevokeRecord> {
            val revoke = Revoke.from(revoked);
            revoke.setRevoked(true);
            return revokeRepository.update(revoke);
    }

    fun getClient(): ApiClient {
        val apiClient = ApiClient()
        apiClient.basePath = ARIES_BASEPATH
        return apiClient;
    }

    fun toRevokeRequest(revoke: RevokeDTO) : RevokeRequest {
        var request = RevokeRequest();
        request.connectionId(revoke.connectionId)
        request.credExId(revoke.credExId)
        request.publish(revoke.publish)
        request.notify(revoke.notify)
        request.threadId(revoke.threadId)
        return request;
    }

}