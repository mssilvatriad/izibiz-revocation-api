package com.certisign.revocation.adapter.repository

import com.certisign.revocation.adapter.repository.model.RevokeRecord
import com.certisign.revocation.domain.exception.AlreadyRegisteredException
import com.certisign.revocation.domain.exception.RevocationNotFoundException
import com.certisign.revocation.domain.model.Revoke
import com.certisign.revocation.domain.model.RevokeRepository
import com.mongodb.client.MongoCollection
import org.bson.Document
import org.springframework.context.annotation.Primary
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Component
import java.util.*

@Component
@Primary
class RevokeRepositoryImpl(
    private val repository: RevokeRepositoryMongo
    ) : RevokeRepository{

    override fun findByCredExId(credExId: String): Optional<RevokeRecord> {
        val revokeRecord = repository.findByCredExId(credExId);
        if (revokeRecord != null) {
            return Optional.of(revokeRecord);
        }
        return Optional.empty();
    }

    override fun findByExpiration(date: Date): List<RevokeRecord> {
        if (date != null) {
            return repository.findByExpiration(date);
        }
        return arrayListOf<RevokeRecord>();
    }

    override fun save(revoke: Revoke): String {
        val revokeRecorded = repository.findByCredExId(revoke.credExId())
        val revokeRecord = RevokeRecord.from(revoke);
        if (revokeRecorded != null) {
            throw AlreadyRegisteredException("This revoke already exists.")
        }
        repository.save(revokeRecord)
        return revokeRecord.id
    }

    override fun findByConnectionId(id: String): Optional<String> {
        TODO("Not yet implemented")
    }

    override fun findAll(): List<RevokeRecord> {
        return repository.findAll();
    }

    override fun update(revoke: Revoke): Optional<RevokeRecord> {
        val revokeRecorded = repository.findByCredExId(revoke.credExId())
        if (revokeRecorded != null) {
            val revokeRecord = RevokeRecord.from(revoke);
            revokeRecord.id = revokeRecorded.id;
            return Optional.of(repository.save(revokeRecord));
        } else {
            throw RevocationNotFoundException("This revoke doesn't exists.")
        }
    }

}