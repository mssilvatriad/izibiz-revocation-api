package com.certisign.revocation.domain.exception

class RetrieveAriesRevokingException (message: String) : Exception(message)