package com.certisign.revocation.domain.exception

class RevocationNotFoundException (message: String) : Exception(message)