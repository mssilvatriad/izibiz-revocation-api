package com.certisign.revocation.domain.exception

class RevokingException (message: String) : Exception(message)