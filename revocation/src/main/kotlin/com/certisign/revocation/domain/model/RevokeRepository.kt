package com.certisign.revocation.domain.model

import com.certisign.revocation.adapter.repository.model.RevokeRecord
import java.util.*
import kotlin.collections.ArrayList

interface RevokeRepository {

    fun findByCredExId(credExId: String): Optional<RevokeRecord>
    fun findByExpiration(date: Date): List<RevokeRecord>
    fun save(revoke: Revoke): String
    fun findByConnectionId(id: String): Optional<String>
    fun findAll(): List<RevokeRecord>
    fun update(revoke: Revoke): Optional<RevokeRecord>

}