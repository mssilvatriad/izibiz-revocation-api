package com.certisign.revocation.domain.exception

class RevokeCredentialAriesException (message: String) : Exception(message)