package com.certisign.revocation.domain.exception

class AlreadyExistsException (message: String) : Exception(message)