package com.certisign.revocation.domain.exception

class AlreadyRegisteredException (message: String) : Exception(message)