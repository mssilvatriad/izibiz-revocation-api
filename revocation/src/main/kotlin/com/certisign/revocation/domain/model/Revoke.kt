package com.certisign.revocation.domain.model

import com.certisign.revocation.adapter.dto.RevokeDTO
import com.certisign.revocation.adapter.repository.model.RevokeRecord
import org.springframework.format.annotation.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

class Revoke(
    private var connectionId: String,
    private var credExId: String,
    private var notify: Boolean,
    private var publish: Boolean,
    private var threadId: String,
    private var revoked: Boolean = false,
    private var expiration: Date
){

    fun connectionId(): String {
        return this.connectionId
    }

    fun notify(): Boolean {
        return this.notify
    }

    fun publish(): Boolean {
        return this.publish
    }

    fun credExId(): String {
        return this.credExId
    }


    fun threadId(): String {
        return this.threadId
    }

    fun revoked() : Boolean {
        return this.revoked
    }

    fun expiration(): Date {
        return this.expiration
    }

    fun setRevoked(revoked: Boolean) {
        this.revoked = revoked
    }

    fun setExpiration(date : Date) {
        this.expiration = date;
    }

    companion object {
        @JvmStatic
        fun from(revokeDto: RevokeDTO): Revoke {

            requireNotNull(revokeDto.credExId) {"Parameter 'revokeDto' has invalid id"}
            requireNotNull(revokeDto.expiration) {"Parameter 'credExpiration' is required"}

            val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            var date = fmt.parse(revokeDto.expiration);

            return Revoke(
                connectionId = revokeDto.connectionId,
                credExId = revokeDto.credExId,
                notify = revokeDto.notify,
                publish = revokeDto.publish,
                threadId = revokeDto.threadId,
                expiration = date,
            )
        }
    }

}