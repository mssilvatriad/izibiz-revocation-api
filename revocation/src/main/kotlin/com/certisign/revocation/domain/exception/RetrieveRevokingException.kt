package com.certisign.revocation.domain.exception

class RetrieveRevokingException (message: String) : Exception(message)