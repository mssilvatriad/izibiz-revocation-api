package com.certisign.revocation.domain.exception

class NewRevocationRegistryException (message: String) : Exception(message)