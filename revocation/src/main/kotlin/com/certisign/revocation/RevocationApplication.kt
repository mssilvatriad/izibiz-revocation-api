package com.certisign.revocation

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RevocationApplication

fun main(args: Array<String>) {
	runApplication<RevocationApplication>(*args)
}
