package com.certisign.revocation.application.schedule

import com.certisign.revocation.adapter.dto.RevokeDTO
import com.certisign.revocation.adapter.service.AriesService
import com.certisign.revocation.adapter.service.RevokeService
import com.certisign.revocation.domain.exception.NewRevocationRegistryException
import com.certisign.revocation.domain.exception.RevokeCredentialAriesException
import org.apache.juli.logging.Log
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger

@Component
class RevocationTask (
    private val revokeService: RevokeService,
    private val ariesService: AriesService
)
{
    @Scheduled(cron = "\${revocation.cron.task}")
    fun revokeCredentials() {
        try {

            LOG.info("-------------SEARCH FOR REVOCATIONS----------------")

            var date = Date()
            var fmt = parseDateFormat(date);

            LOG.info("\nSearch for date: $fmt")

            var revocations = revokeService.revokeCredentialAries(fmt);

            LOG.info("\nRevoked records: $revocations")
            LOG.info("---------------------END SEARCH---------------------")
        } catch (e: RevokeCredentialAriesException) {
            throw NewRevocationRegistryException("Error when revoke the credential on Aries!")
        }
    }

    fun parseDateFormat(date: Date): String {
        val fmt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        var stringDate = fmt.format(date);
        return stringDate;
    }

    companion object {
        val LOG: Logger = Logger.getLogger(RevocationTask::class.java.name)
    }
}