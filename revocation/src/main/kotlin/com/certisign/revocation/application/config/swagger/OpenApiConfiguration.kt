package com.certisign.revocation.application.config.swagger

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfiguration {

    @Bean
    fun openApi(): OpenAPI {
        return OpenAPI().components(Components())
            .info(
                Info().title("iziBiz Revocation API").description(
                    "This documentation shows all methods for manipulating objects from the revocation context."
                )
            )
    }

}