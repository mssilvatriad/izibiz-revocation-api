package com.certisign.revocation.application.controllers

import com.certisign.revocation.adapter.dto.RevokeDTO
import com.certisign.revocation.adapter.service.AriesService
import com.certisign.revocation.adapter.service.RevokeService
import com.certisign.revocation.application.config.swagger.RevokeControllerDoc
import com.certisign.revocation.domain.exception.*
import com.certisign.revocation.domain.model.Revoke
import io.swagger.client.model.RevRegsCreated
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam


@Controller
class RevokeController (
    private val revokeService: RevokeService,
    private val ariesService: AriesService
    ) : RevokeControllerDoc {

    @PostMapping("revoke/save")
    override fun save(@RequestBody revokeDto: RevokeDTO) : ResponseEntity<String> {
        var revoke = Revoke.from(revokeDto);
        val createdRevoke = revokeService.registerRevoke(revoke);
        return ResponseEntity.ok(createdRevoke)
    }

    @GetMapping("revoke")
    override fun getRevokes(): ResponseEntity<ArrayList<RevokeDTO>> {
        val revokes = revokeService.findAll();
        return ResponseEntity.ok(revokes)
    }

    @GetMapping("revoke/expiration")
    override fun getRevocationByExpiration(@RequestParam data : String): ResponseEntity<ArrayList<RevokeDTO>> {
        val revokes = revokeService.findAllExpiration(data)
        return ResponseEntity.ok(revokes)
    }

    @GetMapping("revoke/aries")
    override fun sendRevocationAries(@RequestParam data : String): ResponseEntity<ArrayList<RevokeDTO>> {
        val revokes = revokeService.revokeCredentialAries(data)
        return ResponseEntity.ok(revokes)
    }
}