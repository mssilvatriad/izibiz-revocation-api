package com.certisign.revocation.application.config.swagger

import com.certisign.revocation.adapter.dto.RevokeDTO
import io.swagger.client.model.RevRegsCreated
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

@Tag(name = "Revoke", description = "Features that allow you to query and manipulate Revocations")
interface RevokeControllerDoc {

    @Operation(description = "Save revocation registry to database",
        tags = ["Revoke"],
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "Return id of created object.",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = String::class, type = "String"))
                )]
            )
        ]
    )
    fun save(@RequestBody revokeDto: RevokeDTO) : ResponseEntity<String>

    @Operation(description = "Retrieve all revocations registries",
        tags = ["Revoke"],
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "List of object RevokeDTO",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = RevokeDTO::class, type = "RevokeDTO"))
                )]
            )
        ]
    )
    fun getRevokes(): ResponseEntity<ArrayList<RevokeDTO>>

    @Operation(description = "Retrieve all revocations registries by expiration data",
        tags = ["Revoke"],
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "List of object RevokeDTO",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = RevokeDTO::class, type = "RevokeDTO"))
                )]
            )
        ]
    )
    fun getRevocationByExpiration(@RequestParam data : String): ResponseEntity<ArrayList<RevokeDTO>>

    @Operation(description = "Send a revocation record to aries according to a expiration date parameter",
        tags = ["Revoke"],
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "List of object RevokeDTO",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = RevokeDTO::class, type = "RevokeDTO"))
                )]
            )
        ]
    )
    fun sendRevocationAries(@RequestParam data : String): ResponseEntity<ArrayList<RevokeDTO>>
}