package com.certisign.revocation.adapter.service

import com.certisign.revocation.adapter.repository.model.RevokeRecord
import com.certisign.revocation.domain.exception.AlreadyRegisteredException
import com.certisign.revocation.domain.model.Revoke
import com.certisign.revocation.domain.model.RevokeRepository
import org.junit.Before
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

//@RunWith(MockitoJUnitRunner::class)
class RevokeServiceTest {

//    @Mock
//    lateinit var revokeRepository: RevokeRepository;
//
//    @Mock
//    lateinit var ariesService: AriesService;
//
//    @InjectMocks
//    @Autowired
//    lateinit var revokeService: RevokeService;
//
//    @Before
//    @Throws(Exception::class)
//    fun setup() {
//        MockitoAnnotations.openMocks(this);
//    }
//
//    @Test
//    fun registerRevokeWhenRevokeExist() {
//
//        val revoke = createRevokeObj();
//        val exception = AlreadyRegisteredException("This credential has been registered already");
//        Mockito.`when`(revokeRepository.findByCredExId(revoke.credExId()))
//            .thenThrow(exception);
//
//        val service = revokeService.registerRevoke(revoke);
//
//        assertEquals(service, exception);
//    }
//
//    @Test
//    fun findAll() {
//    }
//
//    @Test
//    fun findAllExpiration() {
//    }
//
//    @Test
//    fun revokeCredentialAries() {
//    }
//
//    @Test
//    fun parseData() {
//    }
//
//    @Test
//    fun parseDateFormat() {
//    }
//
//    fun createRevokeObj(): Revoke {
//        return Revoke("conn123",
//            "cred123",
//            true,
//            true,
//            "thread12",
//            false, Date());
//    }
}